//
// Created by chitchat on 7/3/20.
//

#ifndef RAYLIB_2D_SRC_MAP_H_
#define RAYLIB_2D_SRC_MAP_H_

#include <raymath.h>
#include <vector>
#include "Entity/Entity.h"

class Map {
 public:
  Vector2 mapToRealCoord(Vector2 map);
  Vector2 realToMapCoord(Vector2 real);
  Map();
  bool checkCollision(Entity ent);
  void draw();
  void add(Entity ent);
 private:
  std::vector<Entity> map = {};
};

#endif //RAYLIB_2D_SRC_MAP_H_
