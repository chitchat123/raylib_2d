//
// Created by chitchat on 7/3/20.
//

#ifndef RAYLIB_2D_SRC_GAME_H_
#define RAYLIB_2D_SRC_GAME_H_

#include "Player/Player.h"
#include "Map.h"
class Game {
 private:
  int speed;
  Player player;
  Camera2D camera = { 0 };
  Map map;
 public:
  Game();
  void update();
  void draw();
};

#endif //RAYLIB_2D_SRC_GAME_H_
