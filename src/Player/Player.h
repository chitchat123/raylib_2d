//
// Created by chitchat on 7/3/20.
//

#ifndef RAYLIB_2D_SRC_PLAYER_PLAYER_H_
#define RAYLIB_2D_SRC_PLAYER_PLAYER_H_

#include <raylib.h>
#include "../Entity/Entity.h"
#include "../Map.h"

class Player : public Entity {
 private:
  Map *map = nullptr;
 public:
  void addMap(Map map) { this->map = &map; };
  Player();
  void update();
};

#endif //RAYLIB_2D_SRC_PLAYER_PLAYER_H_
