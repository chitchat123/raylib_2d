//
// Created by chitchat on 7/3/20.
//

#include <cstdio>
#include <raymath.h>
#include "Player.h"
Player::Player() : Entity({500, 400}){

}
void Player::update() {
  int key = GetKeyPressed();
//  if (key) printf("%d\n", key);
  switch (key) {
    case KEY_D:
    case 32 + KEY_D: {
      this->direction(EAST);
      this->moving(true);
    }
      break;
    case KEY_S:
    case 32 + KEY_S: {
      this->direction(SOUTH);
      this->moving(true);
    }
      break;
    case KEY_A:
    case 32 + KEY_A: {
      this->direction(WEST);
      this->moving(true);
    }
      break;
    case KEY_W:
    case 32 + KEY_W: {
      this->direction(NORTH);
      this->moving(true);
    }
      break;
    case 32: this->moving(false);;
  }
  switch (this->direction()) {
    case NORTH:if (this->moving()) setPos(Vector2Add(this->getPos(), {0, -this->getVelocity()}));
      break;
    case EAST:if (this->moving()) setPos(Vector2Add(this->getPos(), {this->getVelocity(), 0}));
      break;
    case SOUTH:if (this->moving()) setPos(Vector2Add(this->getPos(), {0, this->getVelocity()}));
      break;
    case WEST:if (this->moving()) setPos(Vector2Add(this->getPos(), {-this->getVelocity(), 0}));
      break;
  }
}
