//
// Created by chitchat on 7/3/20.
//

#ifndef RAYLIB_2D_SRC_ENTITY_ENTITY_H_
#define RAYLIB_2D_SRC_ENTITY_ENTITY_H_

#include <raylib.h>
#include <cstdint>
enum DIRECTIONS {
  NONE = -1, WEST = 2, NORTH=3, EAST=0, SOUTH=1
};
class Entity {
 private:
  Vector2 position;
  float velocity;
  float size;
  DIRECTIONS direct;
  int fov = 114;
  bool isMoving = false;
 public:
  void moving(bool mov) {this->isMoving = mov;};
  bool moving(){return this->isMoving;}
  int direction() { return this->direct; };
  void direction(DIRECTIONS dir) { this->direct = dir; };
  void setVelocity(float vel) { this->velocity = vel; };
  float getVelocity() { return this->velocity; };
  Vector2 getPos() { return this->position; };
  void setPos(Vector2 pos) { this->position = pos; };
  Entity(Vector2 pos);
  void draw();
};

#endif //RAYLIB_2D_SRC_ENTITY_ENTITY_H_
