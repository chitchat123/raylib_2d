//
// Created by chitchat on 7/3/20.
//

#include <raymath.h>
#include "Entity.h"
#define PI 3.14159265

Entity::Entity(Vector2 pos) : position(pos) {

}
void Entity::draw() {
  DrawCircleV(this->position, 50, MAROON);
  float r = 200.0;
  for (int i = 0; i < this->fov / 2; ++i) {
    for (int dr = 0; dr < r; dr += 10) {
      DrawCircleV(Vector2Add(this->position,
                             {static_cast<float>(cos((i + 90 * this->direct) * PI / 180) * dr),
                              static_cast<float>(sin((i + 90 * this->direct) * PI / 180) * dr)}), 1,
                  RED);
      DrawCircleV(
          Vector2Add(this->position,
                     {static_cast<float>(cos((i + -90 * this->direct) * PI / -180) * dr),
                      static_cast<float>(sin((i + -90 * this->direct) * PI / -180) * dr)}), 1,
          RED);
    }
  }
}
