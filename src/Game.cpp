//
// Created by chitchat on 7/3/20.
//

#include <raylib.h>
#include <cstdio>
#include "Game.h"
Game::Game() {
  this->player.setVelocity(10);
  this->player.setPos({500, 400});
  this->player.addMap(this->map);
  this->map.add(Entity({10, 10}));
  this->camera.target = this->player.getPos();
  this->camera.offset = (Vector2) {400, 225};
  this->camera.rotation = 0.0f;
  this->camera.zoom = 0.8f;
}
void Game::draw() {
  BeginDrawing();
  BeginMode2D(camera);
  ClearBackground(RAYWHITE);
  for (int x = 0; x < 100; ++x) {
    for (int y = 0; y < 100; ++y) {
      if ((x + y) % 2 == 0) DrawRectangle(100 * x, 100 * y, 100, 100, BLACK);
    }
  }
  this->map.draw();
  this->player.draw();
  EndMode2D();
  DrawText(FormatText("x: %f y: %f", this->player.getPos().x, this->player.getPos().y), 1, 1, 10, DARKGRAY);
  DrawFPS(15, 15);

  EndDrawing();
}
void Game::update() {
  this->camera.target = this->player.getPos();
  this->player.update();
}
